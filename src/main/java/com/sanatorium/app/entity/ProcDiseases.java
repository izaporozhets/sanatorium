package com.sanatorium.app.entity;

import com.sanatorium.app.entity.compositeid.ProcDisId;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@IdClass(ProcDisId.class)
@Table(name = "procedures_disease")
public class ProcDiseases {

    @Id
    @Column(name = "procedureid",insertable = false,updatable = false)
    private Integer procedureId;

    @Id
    @Column(name = "diseaseid",insertable = false,updatable = false)
    private Integer diseaseId;

    @ManyToOne
    @JoinColumn(name = "procedureid")
    private Procedure procedureid;

    @ManyToOne
    @JoinColumn(name = "diseaseid")
    private Disease diseaseid;
}

