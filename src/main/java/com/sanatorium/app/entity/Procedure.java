package com.sanatorium.app.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "procedures")
public class Procedure {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "roomid", referencedColumnName = "No")
    private TreatmentRoom treatmentRoom;
}
