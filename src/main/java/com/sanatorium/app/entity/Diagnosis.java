package com.sanatorium.app.entity;

import com.sanatorium.app.entity.compositeid.DiagnosisId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@IdClass(DiagnosisId.class)
@Table(name = "diagnosis")
@AllArgsConstructor
@NoArgsConstructor
public class Diagnosis {

    @Id
    private Long patient_scc;

    @Id
    private Integer disease_id;

    @ManyToOne
    @JoinColumn(name = "patient_scc",insertable = false, updatable = false)
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "disease_id",insertable = false, updatable = false)
    private Disease disease;

}