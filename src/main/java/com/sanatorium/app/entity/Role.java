package com.sanatorium.app.entity;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Role {
    UNKNOWN(Set.of(Permission.UNKNOWN_READ)),
    ADMIN(Set.of(Permission.ADMINISTRATOR_READ, Permission.ADMINISTRATOR_WRITE)),
    DOCTOR(Set.of(Permission.DOCTOR_READ, Permission.DOCTOR_WRITE)),
    PROCEDURE_SPEC(Set.of(Permission.PROCEDURE_SPEC_READ, Permission.PROCEDURE_SPEC_WRITE));

    private final Set<Permission> permissions;

    Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
    }
}
