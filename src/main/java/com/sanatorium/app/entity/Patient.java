package com.sanatorium.app.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Data
@Entity
@Table(name = "`patients`")
public class Patient {

    @Id
    @Column(name = "`sanatorium_cc`")
    private Long sanatorium_cc;

    @Column(name = "`voucher_s`")
    private String voucher_s;

    @Column(name = "`name`")
    private String name;

    @Column(name = "`surname`")
    private String surname;

    @Column(name = "`middlename`")
    private String middlename;

    @Column(name = "`b-day`")
    private Date birthday;

    @Column(name = "`domicile`")
    private String domicile;

    @ManyToOne
    @JoinColumn(name = "`profession_id`")
    private Profession profession;

    @ManyToOne
    @JoinColumn(name = "`doctor_id`")
    private Worker doctor;

    @Column(name = "`arrival_date`")
    private Date arrivalDate;

    @Column(name = "`leave_date`")
    private Date leaveDate;

    @Column(name = "`voucher_date`")
    private Date voucherDate;

    @Column(name = "`corpus`")
    private String corpus;

    @Column(name = "`room`")
    private String room;

    @Column(name = "`place`")
    private String place;

    @Column(name = "`password`")
    private String password;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "patient_scc")
    private List<Diagnosis> diagnoses;

    public String getFullName(){
        return surname +" "+ name +" "+ middlename;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "sanatorium_cc=" + sanatorium_cc +
                ", voucher_s='" + voucher_s + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middlename='" + middlename + '\'' +
                '}';
    }
}
