package com.sanatorium.app.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "workers")
public class Worker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "middlename")
    private String middlename;

    @ManyToOne
    @JoinColumn(name = "`specialityid`")
    private Speciality speciality;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private Role role;

    @ManyToMany
    @JoinTable(name = "workers_rooms",
    joinColumns = @JoinColumn(name = "workerid", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "roomid", referencedColumnName = "No"))
    private Set<TreatmentRoom> treatmentRoom;

    public String getFullName(){
        return surname + " " + name + " " + middlename;
    }

}
