package com.sanatorium.app.entity;

import lombok.Data;
import javax.persistence.*;
import java.sql.Time;

@Table(name = "treatment_rooms")
@Entity
@Data
public class TreatmentRoom {
    @Id
    @Column(name = "No")
    private Integer No;
    @Column(name = "from")
    private Time from;
    @Column(name = "till")
    private Time till;
    @Column(name = "description")
    private String description;

}
