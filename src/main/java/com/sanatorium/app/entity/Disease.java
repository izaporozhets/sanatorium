package com.sanatorium.app.entity;

import lombok.Data;
import java.util.List;
import javax.persistence.*;

@Data
@Entity
@Table(name = "`diseases`")
public class Disease {
    @Id
    @Column(name = "`id`")
    private Integer id;

    @Column(name = "`name`")
    private String name;

    @ManyToMany
    @JoinTable(name = "procedures_disease",
            joinColumns = {@JoinColumn(name = "diseaseid", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "procedureid", referencedColumnName = "id")}
    )
    public List<Procedure> procedureList;
}
