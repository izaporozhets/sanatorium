package com.sanatorium.app.entity;

import com.sanatorium.app.entity.compositeid.ProfDisId;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@IdClass(ProfDisId.class)
@Table(name = "prof_disease")
public class ProfDiseases {

    @Id
    @Column(name = "iddisease",insertable = false,updatable = false)
    private Integer iddisease;

    @Id
    @Column(name = "idprof",insertable = false,updatable = false)
    private Integer idprof;

    @ManyToOne
    @JoinColumn(name = "iddisease")
    private Disease disease;

    @ManyToOne
    @JoinColumn(name = "idprof")
    private Profession profession;

}
