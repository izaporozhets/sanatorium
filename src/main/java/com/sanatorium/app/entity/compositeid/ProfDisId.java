package com.sanatorium.app.entity.compositeid;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfDisId implements Serializable {
    private Integer iddisease;
    private Integer idprof;
}
