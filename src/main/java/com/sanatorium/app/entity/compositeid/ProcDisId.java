package com.sanatorium.app.entity.compositeid;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProcDisId implements Serializable {
    private Integer procedureId;
    private Integer diseaseId;
}