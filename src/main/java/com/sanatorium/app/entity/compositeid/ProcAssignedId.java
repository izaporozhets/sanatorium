package com.sanatorium.app.entity.compositeid;

import lombok.Data;
import java.io.Serializable;

@Data
public class ProcAssignedId implements Serializable {
    private Long sanatorium_cc;
    private Integer procedureid;
}
