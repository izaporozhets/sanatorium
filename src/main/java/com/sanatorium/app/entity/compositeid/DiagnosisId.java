package com.sanatorium.app.entity.compositeid;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

@Data
public class DiagnosisId implements Serializable {
    private Long patient_scc;
    private Integer disease_id;
}

