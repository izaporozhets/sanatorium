package com.sanatorium.app.entity;

public enum Permission {
    UNKNOWN_READ("unknown:read"),
    ADMINISTRATOR_READ("admin:read"),
    ADMINISTRATOR_WRITE("admin:write"),
    DOCTOR_READ("doctor:read"),
    DOCTOR_WRITE("doctor:write"),
    PROCEDURE_SPEC_WRITE("procedureSpec:write"),
    PROCEDURE_SPEC_READ("procedureSpec:read");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
