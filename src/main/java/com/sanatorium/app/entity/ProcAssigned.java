package com.sanatorium.app.entity;

import com.sanatorium.app.entity.compositeid.ProcAssignedId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@IdClass(ProcAssignedId.class)
@Table(name = "`procedures_assigned`")
@AllArgsConstructor
@NoArgsConstructor
public class ProcAssigned {

    @Id
    private Long sanatorium_cc;

    @Id
    private Integer procedureid;

    @ManyToOne
    @JoinColumn(name = "procedureid",insertable = false, updatable = false)
    private Procedure procedure;

    @Column(name = "assigned")
    private Integer assigned;

    @Column(name = "passed")
    private Integer passed;

    @Column(name = "comment")
    private String comment;

    @Column(name = "date")
    private Date date;
}
