package com.sanatorium.app.validator;

import com.sanatorium.app.dto.PatientsQueryDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PatientsQueryDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PatientsQueryDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PatientsQueryDto dto = (PatientsQueryDto)o;
        if(dto.getStartDate() != null && dto.getEndDate() == null || dto.getEndDate() != null && dto.getStartDate() == null){
            errors.rejectValue("startDate", "","Введите дату");
            errors.rejectValue("endDate","","Введите дату");
        }
        if(dto.getStartDate() != null && dto.getEndDate() != null){
            if(dto.getStartDate().getTime() >= dto.getEndDate().getTime()){
                errors.rejectValue("startDate","","Неверный ввод данных");
                errors.rejectValue("endDate","","Неверный ввод данных");
            }
        }
    }
}
