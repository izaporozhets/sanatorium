package com.sanatorium.app.controller;

import com.sanatorium.app.dto.PatientsQueryDto;
import com.sanatorium.app.entity.*;
import com.sanatorium.app.service.*;
import com.sanatorium.app.validator.PatientsQueryDtoValidator;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PatientController {

    private final WorkerService workerService;
    private final DiseaseService diseaseService;
    private final PatientService patientService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final DiagnosisService diagnosisService;
    private final ProfessionService professionService;
    private final PatientsQueryDtoValidator patientsQueryDtoValidator;
    private final AssignedProceduresService assignedProceduresService;

    public PatientController(WorkerService workerService, DiseaseService diseaseService, PatientService patientService, BCryptPasswordEncoder passwordEncoder, AssignedProceduresService assignedProceduresService, DiagnosisService diagnosisService, ProfessionService professionService, PatientsQueryDtoValidator patientsQueryDtoValidator) {
        this.workerService = workerService;
        this.diseaseService = diseaseService;
        this.patientService = patientService;
        this.passwordEncoder = passwordEncoder;
        this.assignedProceduresService = assignedProceduresService;
        this.diagnosisService = diagnosisService;
        this.professionService = professionService;
        this.patientsQueryDtoValidator = patientsQueryDtoValidator;
    }

    @GetMapping("/patient-home")
    public String patientsPage(){
        return "patient/patient-home";
    }

    @GetMapping("*/patient-info/{scc}")
    @PreAuthorize(value = "hasAnyAuthority('doctor:read', 'admin:read')")
    public String getPatientInfo(Model model, @PathVariable("scc") Long scc) {
        model.addAttribute("patient", patientService.findPatientById(scc));
        return "patient/patient-info";
    }

    @GetMapping("*/patient-procedures")
    @PreAuthorize(value = "hasAnyAuthority('doctor:read', 'admin:read')")
    public String getPatientProceduresPost(Model model, @RequestParam(value = "scc") Long scc) {
        List<ProcAssigned> assignedList = assignedProceduresService.findAllByPatientId(scc);
        Patient patient = patientService.findPatientById(scc).get();
        model.addAttribute("diagnoses", diagnosisService.findByPatientId(scc));
        model.addAttribute("patient", patient);
        model.addAttribute("doctor", workerService.findById(patient.getDoctor().getId()).get());
        model.addAttribute("assignedList", assignedList);
        return "doctor/procedures-assigned";
    }

    @GetMapping("/findPatients")
    @PreAuthorize(value = "hasAnyAuthority('doctor:read','admin:read')")
    public String findPatients(Model model, @RequestParam(value = "search")String search){
        if(search.matches(".*\\d.*")){
            model.addAttribute("patientList",patientService.findAllBySccOrVoucher(search));
        }
        else{
            model.addAttribute("patientList",patientService.findAllByNameOrSurnameOrMiddlename(search));
        }
        setInfoForPatientModel(model);
        return "admin/admin-patients";
    }

    @GetMapping("*/find-with-parameters")
    @PreAuthorize(value = "hasAnyAuthority('admin:read', 'doctor:read')")
    public String getPatients(Model model,@ModelAttribute(value = "patientQueryDto")@Valid PatientsQueryDto patientsDto, Errors errors, BindingResult result){
        patientsQueryDtoValidator.validate(patientsDto, result);
        setInfoForPatientModel(model);
        if(errors.hasErrors()){
            return "admin/admin-patients";
        }
        if(patientsDto.getDiseaseId() != null){
            model.addAttribute("patientList", patientService.findAllWithParameters(patientsDto));
        }
        else{
            model.addAttribute("patientList", patientService.findAllWithParametersExceptDiseaseId(patientsDto));
        }
        return "admin/admin-patients";
    }


    @GetMapping("/findDoctorPatients")
    @PreAuthorize(value = "hasAnyAuthority('doctor:read','admin:read')")
    public String findDoctorPatients(Model model, @RequestParam(value = "search")String search){
        if(search.matches(".*\\d.*")){
            model.addAttribute("patientList",patientService.findAllBySccOrVoucher(search));
        }
        else{
            model.addAttribute("patientList",patientService.findAllByNameOrSurnameOrMiddlename(search));
        }
        return "doctor/doctor-patients";
    }

    public void setInfoForPatientModel(Model model){
        List<Patient> patientList = patientService.findAll();
        List<Disease> diseaseList = diseaseService.findAll();
        List<Worker> doctorList = workerService.findAllDoctors();
        List<Profession> professionList = professionService.findAll();
        List<String> domicileList = new ArrayList<>();
        for(Patient patient:patientList){
            domicileList.add(patient.getDomicile());
        }
        model.addAttribute("domicileList", domicileList);
        model.addAttribute("professionList", professionList);
        model.addAttribute("doctorList", doctorList);
        model.addAttribute("diseasesList", diseaseList);
        model.addAttribute("patientQueryDto", new PatientsQueryDto());
    }

    @PostMapping("/search-for-procedures")
    public String searchForProcedures(Model model, @RequestParam("password")String password){
        Patient patient = patientService.findByPassword(password).get();
        List<ProcAssigned> procedureList = assignedProceduresService.findAllByPatientId(patient.getSanatorium_cc());
        model.addAttribute("procedureList", procedureList);
        return "patient/patient-assigned-procedures";
    }
}
