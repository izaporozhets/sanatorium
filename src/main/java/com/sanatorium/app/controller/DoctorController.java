package com.sanatorium.app.controller;

import com.sanatorium.app.dto.PatientDto;
import com.sanatorium.app.entity.*;
import com.sanatorium.app.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.Date;
import java.util.*;

@Controller
public class DoctorController {

    private final WorkerService workerService;
    private final DiseaseService diseaseService;
    private final PatientService patientService;
    private final DiagnosisService diagnosisService;
    private final ProcedureService procedureService;
    private final ProfessionService professionService;
    private final AssignedProceduresService assigned;

    public DoctorController(WorkerService workerService, DiseaseService diseaseService, PatientService patientService, DiagnosisService diagnosisService, ProcedureService procedureService, ProfessionService professionService, AssignedProceduresService assigned) {
        this.workerService = workerService;
        this.diseaseService = diseaseService;
        this.patientService = patientService;
        this.diagnosisService = diagnosisService;
        this.procedureService = procedureService;
        this.professionService = professionService;
        this.assigned = assigned;
    }

    @GetMapping("/doctor-home")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String getDoctorHomePage(Model model) {
        Worker doctor = getAuthenticatedDoctor();
        model.addAttribute("doctor", doctor);
        return "doctor/doctor-home";
    }

    @GetMapping("/doctor-home/patients/{id}")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String getDoctorsPatients(@PathVariable(value = "id") Integer id, Model model) {
        List<Patient> patients = patientService.findAllByDoctorId(id);
        model.addAttribute("patientList", patients);
        return "doctor/doctor-patients";
    }



    @GetMapping("/doctor-home/patient")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String getNewPatientForm(Model model) {
        PatientDto patientDto = new PatientDto();
        patientDto.setDoctorId(getAuthenticatedDoctor().getId());
        model.addAttribute("professions", professionService.findAll());
        model.addAttribute("patientDto", patientDto);
        return "patient/patient-new";
    }


    @GetMapping("/doctor-home/patient/diagnosis")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String assignDiagnosis(Model model, @RequestParam(value = "scc") Long scc) {
        Patient patient = patientService.findPatientById(scc).get();
        model.addAttribute("patient", patient);
        List<Disease> diseases = diseaseService.findAll();
        diseases.removeAll(diseaseService.findAllByPatientId(scc));
        model.addAttribute("diseases", diseases);
        return "doctor/choose-diagnosis";
    }


    @GetMapping("/doctor-home/patient/assign")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String assignProcedure(@RequestParam(value = "diseases") List<Integer> diseasesIds,
                                  @RequestParam(value = "scc") Long scc) {
        saveDiagnosis(diseasesIds, scc);
        return "redirect:/doctor-home/patient/assignMore?scc=" + scc;
    }

    @GetMapping("/doctor-home/patient/assignMore")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String assignProcedureAgain(Model model, @RequestParam(value = "scc") Long scc) {
        Optional<Patient> optPatient = patientService.findPatientById(scc);
        Patient patient = optPatient.get();
        List<Procedure> procedures = procedureService.findAllByPatientId(scc);
        List<Disease> diseases = diseaseService.findAllByPatientId(scc);

        List<Procedure> profProcedures = new ArrayList<>();
        if (patient.getProfession() != null) {
            profProcedures = procedureService.findAllByProfessionId(patient.getProfession().getId());
        }

        Set<Procedure> proceduresToAssign = new HashSet<>();
        for (Disease disease : diseases) {
            proceduresToAssign.addAll(disease.getProcedureList());
        }

        profProcedures.removeAll(procedures);
        proceduresToAssign.removeAll(procedures);
        proceduresToAssign.removeAll(profProcedures);
        model.addAttribute("procedures", proceduresToAssign);
        model.addAttribute("profProcedures", profProcedures);
        model.addAttribute("patientId", scc);
        return "doctor/choose-procedures";
    }


    @GetMapping("/doctor-home/patient/assign/procedures")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String assignProcedureTimes(Model model, @RequestParam(value = "procedures", required = false) List<Integer> procedures,
                                       @RequestParam(value = "profProcedures", required = false) List<Integer> profProcedures,
                                       HttpServletRequest request, @RequestParam(value = "scc") Long scc) {
        if (profProcedures == null && procedures == null) {
            return "redirect:" + request.getHeader("Referer");
        }
        List<Procedure> procedureList = new ArrayList<>();
        List<Procedure> profProcedureList = new ArrayList<>();
        if (procedures != null) {
            for (Integer id : procedures) {
                procedureList.add(procedureService.findById(id));
            }
        }
        if (profProcedures != null) {
            for (Integer id : profProcedures) {
                profProcedureList.add(procedureService.findById(id));
            }
        }
        procedureList.removeAll(profProcedureList);
        model.addAttribute("procedureList", procedureList);
        model.addAttribute("profProcedures", profProcedureList);
        model.addAttribute("patientId", scc);
        return "doctor/assign-procedures";
    }

    @PostMapping("/doctor-home/patient/assign/procedures")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String saveProcedures(@RequestParam(name = "toPass") List<Integer> toPass,
                                 @RequestParam(name = "comment") List<String> comments,
                                 @RequestParam(name = "proceduresIds", required = false) List<Integer> proceduresIds,
                                 @RequestParam(name = "scc") Long scc) {

        Iterator<Integer> toPassIter = toPass.iterator();
        Iterator<String> commentsIter = comments.iterator();
        Iterator<Integer> proceduresIdsIter = proceduresIds.iterator();

        ProcAssigned procAssigned;
        Integer procId;
        while (proceduresIdsIter.hasNext() && toPassIter.hasNext()) {
            procId = proceduresIdsIter.next();
            procAssigned = new ProcAssigned(scc, procId, procedureService.findById(procId), toPassIter.next(), 0, commentsIter.next(), new Date(System.currentTimeMillis()));
            assigned.save(procAssigned);
        }
        return "redirect:/doctor-home/patient-procedures?scc=" + scc;
    }

    @GetMapping("/deleteProcedure")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String deleteProcedure(@RequestParam(value = "scc")Long scc,
                                  @RequestParam(value = "procedureId") Integer procedureId){
                assigned.deleteByProcedureIdAndPatientId(scc, procedureId);
        return "redirect:/doctor-home/patient-procedures?scc="+scc;
    }

    @GetMapping("/doctor-home/patient/diagnoses")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String getPatientDiagnoses(Model model, @RequestParam(name = "scc")Long scc){
        model.addAttribute("patient", patientService.findPatientById(scc).get());
        model.addAttribute("diseases",diseaseService.findAllByPatientId(scc));
        return "doctor/edit-diagnoses";
    }

    private void saveDiagnosis(List<Integer> diseasesIds, Long scc) {
        Patient patient = patientService.findPatientById(scc).get();
        Diagnosis diagnosis;
        for (Integer id : diseasesIds) {
            diagnosis = new Diagnosis();
            diagnosis.setDisease_id(id);
            diagnosis.setPatient_scc(scc);
            diagnosis.setPatient(patient);
            diagnosis.setDisease(diseaseService.findById(id));
            diagnosisService.save(diagnosis);
        }
    }

    @PostMapping(value = "/savePatient")
    @PreAuthorize(value = "hasAuthority('doctor:write')")
    public String savePatient(@Valid @ModelAttribute("patientDto") PatientDto patientDto,  Errors errors, Model model){
        if(errors.hasErrors()){
            model.addAttribute("professions", professionService.findAll());
            return "patient/patient-new";
        }
        Patient patient = new Patient();
        patient.setSanatorium_cc(Long.valueOf(patientDto.getScc()));
        patient.setVoucher_s(patientDto.getVoucher_s());
        patient.setName(patientDto.getName());
        patient.setSurname(patientDto.getSurname());
        patient.setMiddlename(patientDto.getMiddlename());
        patient.setBirthday(patientDto.getBirthday());
        patient.setDomicile(patientDto.getDomicile());
        patient.setProfession(professionService.findById(patientDto.getProfessionId()));
        Worker worker = workerService.findById(patientDto.getDoctorId()).get();
        patient.setDoctor(worker);
        patient.setArrivalDate(patientDto.getArrivalDate());
        patient.setLeaveDate(patientDto.getLeaveDate());
        patient.setVoucherDate(patientDto.getVoucherDate());
        patient.setCorpus(patientDto.getCorpus());
        patient.setRoom(patientDto.getRoom());
        patient.setPlace(patientDto.getPlace());
        patient.setPassword(patientDto.getPassword());
        patientService.save(patient);
        return "redirect:/doctor-home";
    }

    @PostMapping("/deleteDiagnosis")
    public String deleteDiagnosis(@RequestParam(value = "scc")Long scc, @RequestParam(value = "diseaseId")Integer diseaseId){
        diagnosisService.deleteByPatientsIdAndDiseaseId(scc,diseaseId);
        return "redirect:/doctor-home/patient/diagnoses" + "?scc="+scc;
    }

    private Worker getAuthenticatedDoctor(){
        User doctorUserDetails = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return workerService.findByLogin(doctorUserDetails.getUsername()).get();
    }
}
