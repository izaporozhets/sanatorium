package com.sanatorium.app.controller;
import com.sanatorium.app.service.WorkerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

@Controller
public class MainController {

    private final WorkerService workerService;

    public MainController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping("/")
    public String index(Authentication authentication){
        if(authentication != null && authentication.isAuthenticated()){
            return "redirect:/home";
        }
        return "index";
    }

    @GetMapping("/home")
    public String home(Authentication authentication){

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        for (GrantedAuthority authority : authorities) {
            if (authority.getAuthority().contains("doctor")) {
                return "redirect:/doctor-home";
            }
            if (authority.getAuthority().contains("admin")) {
                return "redirect:/admin-home";
            }
            if (authority.getAuthority().contains("procedureSpec")) {
                return "redirect:/choose-room";
            }
        }
        return "index";
    }
}
