package com.sanatorium.app.controller;

import com.sanatorium.app.dto.PatientsQueryDto;
import com.sanatorium.app.entity.*;
import com.sanatorium.app.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {

    private final WorkerService workerService;
    private final PatientService patientService;
    private final DiseaseService diseaseService;
    private final ProcedureService procedureService;
    private final ProfessionService professionService;
    private final TreatmentRoomService treatmentRoomService;

    public AdminController(WorkerService workerService, PatientService patientService, DiseaseService diseaseService, ProcedureService procedureService, ProfessionService professionService, TreatmentRoomService treatmentRoomService) {
        this.workerService = workerService;
        this.patientService = patientService;
        this.diseaseService = diseaseService;
        this.procedureService = procedureService;
        this.professionService = professionService;
        this.treatmentRoomService = treatmentRoomService;
    }

    @GetMapping("/admin-home")
    @PreAuthorize(value = "hasAuthority('admin:write')")
    public String getAdminHomePage() {
        return "admin/admin-home";
    }

    @GetMapping("/admin-home/patients")
    @PreAuthorize(value = "hasAuthority('admin:read')")
    public String getAllPatients(Model model) {
        List<Patient> patientList = patientService.findAll();
        List<Disease> diseaseList = diseaseService.findAll();
        List<Worker> doctorList = workerService.findAllDoctors();
        List<Profession> professionList = professionService.findAll();
        List<String> domicileList = new ArrayList<>();
        for(Patient patient:patientList){
            domicileList.add(patient.getDomicile());
        }
        model.addAttribute("domicileList", domicileList);
        model.addAttribute("professionList", professionList);
        model.addAttribute("doctorList", doctorList);
        model.addAttribute("patientList", patientList);
        model.addAttribute("diseasesList", diseaseList);
        model.addAttribute("patientQueryDto", new PatientsQueryDto());
        return "admin/admin-patients";
    }

    @GetMapping("/admin-home/workers")
    @PreAuthorize(value = "hasAuthority('admin:read')")
    public String getAllWorkers(Model model) {
        List<Worker> workerList = workerService.findAll();
        model.addAttribute("workerList", workerList);
        return "admin/sanatorium-staff";
    }

    @GetMapping("/admin-home/rooms")
    @PreAuthorize(value = "hasAuthority('admin:read')")
    public String getAllRooms(Model model){
        List<TreatmentRoom> treatmentRooms = treatmentRoomService.findAll();
        model.addAttribute("treatmentRooms", treatmentRooms);
        return "admin/treatment-rooms";
    }

    @GetMapping("/admin-home/diseases")
    @PreAuthorize(value = "hasAuthority('admin:read')")
    public String getAllDiseases(Model model){
        List<Disease> diseaseList = diseaseService.findAll();
        model.addAttribute("diseaseList", diseaseList);
        return "admin/admin-diseases";
    }
}
