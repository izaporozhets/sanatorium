package com.sanatorium.app.controller;

import com.sanatorium.app.entity.*;
import com.sanatorium.app.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class SpecDocController {

    private final WorkerService workerService;
    private final PatientService patientService;
    private final AssignedProceduresService proceduresAssignedService;
    private final TreatmentRoomService treatmentRoomService;

    public SpecDocController(PatientService patientService, WorkerService workerService, ProcedureService procedureService, AssignedProceduresService proceduresAssignedService, TreatmentRoomService treatmentRoomService) {
        this.patientService = patientService;
        this.workerService = workerService;
        this.proceduresAssignedService = proceduresAssignedService;
        this.treatmentRoomService = treatmentRoomService;
    }

    @GetMapping("/choose-room")
    @PreAuthorize(value = "hasAuthority('procedureSpec:write')")
    public String chooseRoom(Model model){
        Worker specDoc = getAuthenticatedSpecDoctor();
        List<TreatmentRoom> treatmentRooms = treatmentRoomService.findByWorkerId(specDoc.getId());
        if(treatmentRooms.size() == 1){
            return "redirect:/specialist-home?room="+treatmentRooms.get(0).getNo();
        }
        model.addAttribute("treatmentRooms", treatmentRooms);
        return "specialist/choose-treatmentroom";
    }

    @GetMapping("/specialist-home/patient")
    @PreAuthorize(value = "hasAuthority('procedureSpec:write')")
    public String patientsProcedures(Model model,@RequestParam("room")Integer room,@RequestParam("scc")Long scc){
        Patient patient = patientService.findPatientById(scc).get();
        List<ProcAssigned> procedureList = proceduresAssignedService.findAllByPatientId(patient.getSanatorium_cc());
        model.addAttribute("patient", patient);
        model.addAttribute("roomNo", room);
        model.addAttribute("procedureList", procedureList);
        return "specialist/patient-proc-info";
    }

    @GetMapping("/specialist-home")
    @PreAuthorize(value = "hasAuthority('procedureSpec:read')")
    public String getSpecialistHomePage(Model model, @RequestParam("room")Integer room){
        List<Patient> patientList = patientService.findAllByTreatmentRoom(room);
        model.addAttribute("roomNo", room);
        model.addAttribute("patientList", patientList);
        return "specialist/specialist-home";
    }

    @GetMapping("/procedure-finished")
    @PreAuthorize(value = "hasAuthority('procedureSpec:write')")
    public String procedureFinished(@RequestParam("procedureId")Integer procedureId,@RequestParam("room")Integer room,@RequestParam("scc")Long scc){
        ProcAssigned procAssigned = proceduresAssignedService.findByProcedureIdAndPatientId(procedureId, scc);
        procAssigned.setPassed(procAssigned.getPassed() + 1);
        proceduresAssignedService.save(procAssigned);
        return "redirect:/specialist-home/patient?room="+room+"&scc="+scc;
    }

    private Worker getAuthenticatedSpecDoctor(){
        User doctorUserDetails = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return workerService.findByLogin(doctorUserDetails.getUsername()).get();
    }
}
