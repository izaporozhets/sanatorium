package com.sanatorium.app.security;

import com.sanatorium.app.entity.Worker;
import com.sanatorium.app.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("workerDetailsServiceImpl")
public class WorkerDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private WorkerRepository workerRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Worker worker = workerRepository.findByLogin(login).orElseThrow(() -> new UsernameNotFoundException("User doesn't exist"));
        return MyUserDetails.fromWorker(worker);
    }
}
