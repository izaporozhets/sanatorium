package com.sanatorium.app.repository;
import java.util.Optional;

import com.sanatorium.app.entity.Worker;
import org.hibernate.jdbc.Work;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface WorkerRepository extends JpaRepository<Worker, Long> {
    Optional<Worker> findByLogin(String login);
    Optional<Worker> findById(Integer id);
    @Query("select w from Worker w where w.speciality.id = 1")
    List<Worker> findAlLDoctors();
}
