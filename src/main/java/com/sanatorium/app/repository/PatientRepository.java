package com.sanatorium.app.repository;

import com.sanatorium.app.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    @Query(value = "select * from patients where cast(patients.sanatorium_cc as char) like concat('%',?1,'%') or patients.voucher_s like concat('%',?1,'%')", nativeQuery = true)
    List<Patient> findAllBySccOrVoucher(String scc);

    @Query(value = "select p from Patient p where p.name like %:input% or p.surname like %:input% or p.middlename like %:input%")
    List<Patient> findAllByPatientNameOrSurnameOrMiddlename(@Param("input") String input);

    @Query("select p from Patient p join Diagnosis d on d.patient_scc = p.sanatorium_cc and d.disease_id = :disease")
    List<Patient> findAllByDiseaseId(@Param("disease") Integer disease);

    @Query("select p from Patient p where p.doctor.id = :id")
    List<Patient> findAllByDoctorId(@Param("id") Integer id);

    @Query("select p from Patient p where p.profession.id = :id")
    List<Patient> findAllByProfessionId(@Param("id")Integer id);

    @Query("select p from Patient p where p.arrivalDate >= :startDate and p.leaveDate <= :endDate")
    List<Patient> findAllByRangeDate(@Param("startDate")Date startDate, @Param("endDate")Date endDate);

    List<Patient> findAllByBirthday(Date birthday);

    @Query("select p from Patient p where p.domicile like %:domicile%")
    List<Patient> findAllByDomicile(@Param("domicile")String domicile);

    @Query(value = "select * from patients join diagnosis d on patients.sanatorium_cc = d.patient_scc " +
            "and d.disease_id = ?1 where (?2 is null or domicile = ?2) " +
            "and (?3 is null or profession_id = ?3) " +
            "and (?4 is null or leave_date >= ?4) " +
            "and (?5 is null or arrival_date <= ?5) " +
            "and (?6 is null or `b-day` = ?6) " +
            "and (?7 is null or doctor_id = ?7)",nativeQuery = true)
    List<Patient> findAllPatientsWithParameters(Integer diseaseId,String domicile ,Integer professionId, Date fromDate, Date tillDate,  Date birthday,Integer doctorId);

    @Query(value = "select * from patients where (?1 is null or domicile = ?1) " +
            "and (?2 is null or profession_id = ?2) " +
            "and (?3 is null or leave_date >= ?3) " +
            "and (?4 is null or arrival_date <= ?4) " +
            "and (?5 is null or `b-day` = ?5) " +
            "and (?6 is null or doctor_id = ?6)", nativeQuery = true)
    List<Patient> findAllPatientsWithParametersExceptDisease(String domicile ,Integer professionId, Date fromDate, Date tillDate,  Date birthday,Integer doctorId);

    @Query(value = "select * from patients join procedures_assigned pa on patients.sanatorium_cc = pa.sanatorium_cc join procedures p on p.id = pa.procedureid where roomid = ?", nativeQuery = true)
    List<Patient> findAllByTreatmentRoom(Integer treatmentRoomId);

    @Query(value = "select p from Patient p where p.password = ?1")
    Optional<Patient> findByPassword(String password);
}
