package com.sanatorium.app.repository;

import com.sanatorium.app.entity.ProcAssigned;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface AssignedProceduresRepository extends JpaRepository<ProcAssigned, Long> {
    @Query(value = "select * from sanatorium.patients join sanatorium.procedures_assigned pa on sanatorium.patients.sanatorium_cc = pa.sanatorium_cc where sanatorium.patients.sanatorium_cc = ?", nativeQuery = true)
    List<ProcAssigned> findAllByPatientId(Long id);

    @Modifying
    @Transactional
    @Query(value = "delete from procedures_assigned where procedures_assigned.sanatorium_cc = ?1 and procedures_assigned.procedureid = ?2", nativeQuery = true)
    void deleteByPatientIdAndProcedureId(Long scc, Integer procedureId);

    @Query("select p from ProcAssigned p where p.procedureid = :procedureId and p.sanatorium_cc = :scc")
    ProcAssigned findByProcedureIdAndPatientId(@Param(value = "procedureId") Integer procedureId,@Param(value = "scc") Long scc);
}
