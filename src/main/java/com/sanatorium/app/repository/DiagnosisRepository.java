package com.sanatorium.app.repository;

import com.sanatorium.app.entity.Diagnosis;
import com.sanatorium.app.entity.compositeid.DiagnosisId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DiagnosisRepository extends JpaRepository<Diagnosis, DiagnosisId> {
    @Query("select d from Diagnosis d where d.patient_scc = ?1")
    List<Diagnosis> findAllByPatientId(Long scc);
    @Modifying
    @Transactional
    @Query("delete from Diagnosis d where d.patient_scc = :scc and d.disease_id = :diseaseId")
    void deleteByPatient_sccAndDisease_id(@Param("scc") Long scc,@Param("diseaseId") Integer diseaseId);
}
