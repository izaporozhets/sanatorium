package com.sanatorium.app.repository;

import com.sanatorium.app.entity.Procedure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProceduresRepository extends JpaRepository<Procedure, Integer> {
    @Query(value = "select proc from Procedure proc join ProcDiseases procDiseases on procDiseases.procedureId = proc.id join ProfDiseases profDisease on profDisease.iddisease = procDiseases.procedureId where profDisease.idprof = ?1")
    List<Procedure> findAllByProfessionId(Integer id);
    @Query(value = "select proc from Procedure proc join ProcAssigned procAssigned on procAssigned.procedureid = proc.id join Patient patient on procAssigned.sanatorium_cc = patient.sanatorium_cc where patient.sanatorium_cc = ?1")
    List<Procedure> findAllByPatientId(Long scc);
}
