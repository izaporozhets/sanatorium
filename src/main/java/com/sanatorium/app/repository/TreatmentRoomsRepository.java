package com.sanatorium.app.repository;

import com.sanatorium.app.entity.TreatmentRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TreatmentRoomsRepository extends JpaRepository<TreatmentRoom, Integer> {
    @Query(value = "select * from treatment_rooms join workers_rooms wr on treatment_rooms.No = wr.roomid where wr.workerid = ?1", nativeQuery = true)
    List<TreatmentRoom> findByWorkerId(Integer workerId);
}
