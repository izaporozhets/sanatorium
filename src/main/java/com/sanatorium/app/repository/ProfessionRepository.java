package com.sanatorium.app.repository;

import com.sanatorium.app.entity.Patient;
import com.sanatorium.app.entity.Profession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfessionRepository extends JpaRepository<Profession, Integer> {
    @Query("select p from Profession p join Patient patient on p.id = patient.profession.id and patient.sanatorium_cc = ?1")
    Optional<Profession> findByPatient(Long scc);
}
