package com.sanatorium.app.repository;

import com.sanatorium.app.entity.Disease;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DiseaseRepository extends JpaRepository<Disease, Integer> {
    @Query(value = "SELECT diseases.id, diseases.name FROM diseases JOIN prof_disease pd on diseases.id = pd.iddisease where pd.idprof = ?1", nativeQuery = true)
    List<Disease> findDiseaseByPatientProfession(Long scc);
    @Query(value = "SELECT diseases.id, diseases.name FROM diseases join diagnosis d on diseases.id = d.disease_id join patients p on p.sanatorium_cc = d.patient_scc where p.sanatorium_cc = ?1",nativeQuery = true)
    List<Disease> findAllByPatientId(Long scc);
}
