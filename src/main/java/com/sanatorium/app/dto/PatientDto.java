package com.sanatorium.app.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
public class PatientDto {

    @NotNull
    private Long scc;
    @NotBlank
    private String voucher_s;
    @NotBlank(message = "Введите имя")
    private String name;
    @NotBlank(message = "Введите фамилию")
    private String surname;
    @NotBlank(message = "Введите отчество")
    private String  middlename;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @NotNull(message = "Введите дату")
    private Date birthday;
    @NotBlank(message = "Введите город проживания пцаента")
    private String domicile;
    @NotNull(message = "Выберите профессию")
    private Integer professionId;

    private Integer doctorId;

    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @NotNull(message = "Введите дату приезда")
    private Date arrivalDate;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    @NotNull(message = "Введите дату выезда")
    private Date leaveDate;
    @NotNull(message = "Введите дату получения путевки")
    @DateTimeFormat(pattern="yyyy-mm-dd")
    private Date voucherDate;
    @NotBlank(message = "Введите номер корпуса")
    private String corpus;
    @NotBlank(message = "Введите номер комнаты")
    private String room;
    @NotBlank(message = "Введите номер места")
    @Size(max = 3, message = "Данное поле не может содержать более 3х символов")
    private String place;
    @NotBlank(message = "Введите пароль для пациента")
    @Size(min = 5, max = 8, message = "Пароль должен содержать от 5 до 8 символов")
    private String password;

}
