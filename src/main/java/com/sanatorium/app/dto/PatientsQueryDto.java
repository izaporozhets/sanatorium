package com.sanatorium.app.dto;

import lombok.Data;

import java.util.Date;

@Data
public class PatientsQueryDto {
    private Integer diseaseId;
    private Integer doctorId;
    private Integer professionId;
    private Date startDate;
    private Date endDate;
    private Date birthday;
    private String domicile;
}
