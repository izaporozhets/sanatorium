package com.sanatorium.app.service;
import com.sanatorium.app.entity.Procedure;
import java.util.List;

public interface ProcedureService{
    List<Procedure> findAll();
    List<Procedure> findAllByPatientId(Long scc);
    List<Procedure> findAllByProfessionId(Integer id);
    Procedure findById(Integer id);
}
