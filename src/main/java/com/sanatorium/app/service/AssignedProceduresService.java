package com.sanatorium.app.service;

import java.util.List;
import com.sanatorium.app.entity.ProcAssigned;

public interface AssignedProceduresService {
    List<ProcAssigned> findAllByPatientId(Long id);
    ProcAssigned findByProcedureIdAndPatientId(Integer procedureId, Long scc);
    void save(ProcAssigned procAssigned);
    void deleteByProcedureIdAndPatientId(Long scc,Integer procedureId);
}
