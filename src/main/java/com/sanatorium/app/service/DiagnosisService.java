package com.sanatorium.app.service;

import com.sanatorium.app.entity.Diagnosis;
import java.util.List;

public interface DiagnosisService {
    List<Diagnosis> findByPatientId(Long scc);
    void save(Diagnosis diagnosis);
    void deleteByPatientsIdAndDiseaseId(Long scc, Integer diseaseId);
}
