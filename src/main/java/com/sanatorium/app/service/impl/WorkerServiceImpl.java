package com.sanatorium.app.service.impl;

import com.sanatorium.app.entity.Worker;
import com.sanatorium.app.repository.WorkerRepository;
import com.sanatorium.app.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerServiceImpl implements WorkerService {

    @Autowired
    private WorkerRepository workerRepository;

    @Override
    public List<Worker> findAll() {
        return workerRepository.findAll();
    }

    @Override
    public Optional<Worker> findByLogin(String login) {
        return workerRepository.findByLogin(login);
    }

    @Override
    public Optional<Worker> findById(Integer id) {
        return workerRepository.findById(id);
    }

    @Override
    public List<Worker> findAllDoctors() {
        return workerRepository.findAlLDoctors();
    }


}
