package com.sanatorium.app.service.impl;

import com.sanatorium.app.entity.Disease;
import com.sanatorium.app.repository.DiseaseRepository;
import com.sanatorium.app.service.DiseaseService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiseaseServiceImpl implements DiseaseService {

    private final DiseaseRepository diseaseRepository;

    public DiseaseServiceImpl(DiseaseRepository diseaseRepository) {
        this.diseaseRepository = diseaseRepository;
    }

    @Override
    public Disease findById(Integer id) {
        return diseaseRepository.findById(id).get();
    }

    @Override
    public List<Disease> findDiseasesByPatientProfession(Long scc) {
        return null;
    }

    @Override
    public List<Disease> findAllByPatientId(Long scc) {
        return diseaseRepository.findAllByPatientId(scc);
    }

    @Override
    public List<Disease> findAll() {
        return diseaseRepository.findAll();
    }
}
