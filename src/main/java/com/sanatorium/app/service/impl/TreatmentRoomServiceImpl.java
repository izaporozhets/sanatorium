package com.sanatorium.app.service.impl;

import com.sanatorium.app.entity.TreatmentRoom;
import com.sanatorium.app.repository.TreatmentRoomsRepository;
import com.sanatorium.app.service.TreatmentRoomService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TreatmentRoomServiceImpl implements TreatmentRoomService {

    private final TreatmentRoomsRepository treatmentRoomsRepository;

    public TreatmentRoomServiceImpl(TreatmentRoomsRepository treatmentRoomsRepository) {
        this.treatmentRoomsRepository = treatmentRoomsRepository;
    }

    @Override
    public List<TreatmentRoom> findAll() {
        return treatmentRoomsRepository.findAll();
    }

    @Override
    public List<TreatmentRoom> findByWorkerId(Integer id) {
        return treatmentRoomsRepository.findByWorkerId(id);
    }
}
