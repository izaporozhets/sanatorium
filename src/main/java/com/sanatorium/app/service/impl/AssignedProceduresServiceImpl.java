package com.sanatorium.app.service.impl;

import com.sanatorium.app.entity.ProcAssigned;
import com.sanatorium.app.repository.AssignedProceduresRepository;
import com.sanatorium.app.service.AssignedProceduresService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssignedProceduresServiceImpl implements AssignedProceduresService {
    private final AssignedProceduresRepository assignedProceduresRepository;

    public AssignedProceduresServiceImpl(AssignedProceduresRepository assignedProceduresRepository) {
        this.assignedProceduresRepository = assignedProceduresRepository;
    }

    @Override
    public List<ProcAssigned> findAllByPatientId(Long id) {
        return assignedProceduresRepository.findAllByPatientId(id);
    }

    @Override
    public ProcAssigned findByProcedureIdAndPatientId(Integer procedureId, Long scc) {
        return assignedProceduresRepository.findByProcedureIdAndPatientId(procedureId, scc);
    }

    @Override
    public void save(ProcAssigned procAssigned) {
        assignedProceduresRepository.save(procAssigned);
    }

    @Override
    public void deleteByProcedureIdAndPatientId( Long scc, Integer procedureId) {
        assignedProceduresRepository.deleteByPatientIdAndProcedureId(scc,procedureId);
    }

}
