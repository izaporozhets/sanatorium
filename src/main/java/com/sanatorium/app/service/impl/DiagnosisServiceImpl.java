package com.sanatorium.app.service.impl;

import com.sanatorium.app.entity.Diagnosis;
import com.sanatorium.app.repository.DiagnosisRepository;
import com.sanatorium.app.service.DiagnosisService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class DiagnosisServiceImpl implements DiagnosisService {

    private final DiagnosisRepository diagnosisRepository;

    public DiagnosisServiceImpl(DiagnosisRepository diagnosisRepository) {
        this.diagnosisRepository = diagnosisRepository;
    }

    @Override
    public List<Diagnosis> findByPatientId(Long scc) {
        return diagnosisRepository.findAllByPatientId(scc);
    }

    @Override
    public void save(Diagnosis diagnosis) {
        diagnosisRepository.save(diagnosis);
    }

    @Override
    public void deleteByPatientsIdAndDiseaseId(Long scc, Integer diseaseId) {
        diagnosisRepository.deleteByPatient_sccAndDisease_id(scc, diseaseId);
    }
}
