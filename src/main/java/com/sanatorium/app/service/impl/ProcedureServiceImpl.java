package com.sanatorium.app.service.impl;

import com.sanatorium.app.entity.Procedure;
import com.sanatorium.app.repository.ProceduresRepository;
import com.sanatorium.app.service.ProcedureService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcedureServiceImpl implements ProcedureService {

    private final ProceduresRepository proceduresRepository;

    public ProcedureServiceImpl(ProceduresRepository proceduresRepository) {
        this.proceduresRepository = proceduresRepository;
    }

    @Override
    public List<Procedure> findAll() {
        return proceduresRepository.findAll();
    }

    @Override
    public List<Procedure> findAllByPatientId(Long scc) {
        return proceduresRepository.findAllByPatientId(scc);
    }

    @Override
    public List<Procedure> findAllByProfessionId(Integer id) {
        return proceduresRepository.findAllByProfessionId(id);
    }

    @Override
    public Procedure findById(Integer id) {
        return proceduresRepository.getOne(id);
    }
}
