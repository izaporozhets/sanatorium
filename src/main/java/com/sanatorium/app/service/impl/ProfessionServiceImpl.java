package com.sanatorium.app.service.impl;

import com.sanatorium.app.entity.Profession;
import com.sanatorium.app.repository.ProfessionRepository;
import com.sanatorium.app.service.ProfessionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfessionServiceImpl implements ProfessionService {

    private final ProfessionRepository professionRepository;

    public ProfessionServiceImpl(ProfessionRepository professionRepository) {
        this.professionRepository = professionRepository;
    }

    @Override
    public List<Profession> findAll() {
        return professionRepository.findAll();
    }

    @Override
    public Profession findByPatientId(Long scc) {
        return professionRepository.findByPatient(scc).get();
    }

    @Override
    public Profession findById(Integer id) {
        return professionRepository.getOne(id);
    }
}
