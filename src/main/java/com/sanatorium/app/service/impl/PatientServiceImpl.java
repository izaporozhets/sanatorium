package com.sanatorium.app.service.impl;

import com.sanatorium.app.dto.PatientsQueryDto;
import com.sanatorium.app.entity.Patient;
import com.sanatorium.app.repository.PatientRepository;
import com.sanatorium.app.service.PatientService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public List<Patient> findAll() {
        return patientRepository.findAll();
    }

    @Override
    public List<Patient> findAllBySccOrVoucher(String scc) {
        return patientRepository.findAllBySccOrVoucher(scc);
    }

    @Override
    public List<Patient> findAllByNameOrSurnameOrMiddlename(String input) {
        return patientRepository.findAllByPatientNameOrSurnameOrMiddlename(input);
    }

    @Override
    public List<Patient> findAllByDiseaseId(Integer diseaseId) {
        return patientRepository.findAllByDiseaseId(diseaseId);
    }

    @Override
    public List<Patient> findAllByProfessionId(Integer professionId) {
        return patientRepository.findAllByProfessionId(professionId);
    }

    @Override
    public List<Patient> findAllByDateRange(Date from, Date till) {
        return patientRepository.findAllByRangeDate(from, till);
    }

    @Override
    public List<Patient> findAllByBirthday(Date birthday) {
        return patientRepository.findAllByBirthday(birthday);
    }

    @Override
    public List<Patient> findAllByDomicile(String domicile) {
        return patientRepository.findAllByDomicile(domicile);
    }

    @Override
    public List<Patient> findAllWithParametersExceptDiseaseId(PatientsQueryDto patientsQueryDto) {
        if(patientsQueryDto.getDomicile().isEmpty()){
            patientsQueryDto.setDomicile(null);
        }
        return patientRepository.findAllPatientsWithParametersExceptDisease(patientsQueryDto.getDomicile(),patientsQueryDto.getProfessionId(), patientsQueryDto.getStartDate(), patientsQueryDto.getEndDate(), patientsQueryDto.getBirthday(), patientsQueryDto.getDoctorId());
    }

    @Override
    public List<Patient> findAllWithParameters(PatientsQueryDto patientsQueryDto) {
        if(patientsQueryDto.getDomicile().isEmpty()){
            patientsQueryDto.setDomicile(null);
        }
        return patientRepository.findAllPatientsWithParameters(patientsQueryDto.getDiseaseId(), patientsQueryDto.getDomicile(), patientsQueryDto.getProfessionId(), patientsQueryDto.getStartDate(), patientsQueryDto.getEndDate(), patientsQueryDto.getBirthday(), patientsQueryDto.getDoctorId());
    }

    @Override
    public List<Patient> findAllByDoctorId(Integer id) {
        return patientRepository.findAllByDoctorId(id);
    }

    @Override
    public List<Patient> findAllByTreatmentRoom(Integer id) {
        return patientRepository.findAllByTreatmentRoom(id);
    }

    @Override
    public Optional<Patient> findPatientById(Long scc) {
        return patientRepository.findById(scc);
    }

    @Override
    public Optional<Patient> findByPassword(String password) {
        return patientRepository.findByPassword(password);
    }

    @Override
    public void save(Patient patient) {
        patientRepository.save(patient);
    }
}
