package com.sanatorium.app.service;

import com.sanatorium.app.entity.Profession;
import java.util.List;

public interface ProfessionService {
    List<Profession> findAll();
    Profession findByPatientId(Long scc);
    Profession findById(Integer id);
}
