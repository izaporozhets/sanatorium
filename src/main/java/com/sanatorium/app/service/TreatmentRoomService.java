package com.sanatorium.app.service;

import com.sanatorium.app.entity.TreatmentRoom;
import java.util.List;

public interface TreatmentRoomService {
    List<TreatmentRoom> findAll();
    List<TreatmentRoom> findByWorkerId(Integer id);
}
