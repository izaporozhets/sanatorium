package com.sanatorium.app.service;

import com.sanatorium.app.entity.Disease;
import java.util.List;

public interface DiseaseService {
    Disease findById(Integer id);
    List<Disease> findDiseasesByPatientProfession(Long scc);
    List<Disease> findAllByPatientId(Long scc);
    List<Disease> findAll();
}
