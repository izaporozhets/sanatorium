package com.sanatorium.app.service;

import com.sanatorium.app.dto.PatientsQueryDto;
import com.sanatorium.app.entity.Patient;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PatientService {
    List<Patient> findAll();
    List<Patient> findAllBySccOrVoucher(String scc);
    List<Patient> findAllByNameOrSurnameOrMiddlename(String input);
    List<Patient> findAllByDiseaseId(Integer diseaseId);
    List<Patient> findAllByProfessionId(Integer professionId);
    List<Patient> findAllByDateRange(Date from, Date till);
    List<Patient> findAllByBirthday(Date birthday);
    List<Patient> findAllByDomicile(String domicile);
    List<Patient> findAllWithParametersExceptDiseaseId(PatientsQueryDto patientsQueryDto);
    List<Patient> findAllWithParameters(PatientsQueryDto patientsQueryDto);
    List<Patient> findAllByDoctorId(Integer id);
    List<Patient> findAllByTreatmentRoom(Integer id);
    Optional<Patient> findPatientById(Long scc);
    Optional<Patient> findByPassword(String password);
    void save(Patient patient);
}
