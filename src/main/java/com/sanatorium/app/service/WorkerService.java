package com.sanatorium.app.service;

import com.sanatorium.app.entity.Worker;

import java.util.List;
import java.util.Optional;

public interface WorkerService {
    List<Worker> findAll();
    Optional<Worker> findByLogin(String login);
    Optional<Worker> findById(Integer id);
    List<Worker> findAllDoctors();
}
